FROM debian:stretch-slim
ARG DEBIAN_FRONTEND=noninteractive
ARG UID
ARG GID
RUN apt-get update && apt-get install -y -q --no-install-recommends ca-certificates git make \
      python-pip python-setuptools \
      python-cherrypy3 python-concurrent.futures python-httplib2 python-ipaddr python-iso8601 \
      python-jinja2 python-lxml python-publicsuffix python-redis python-requests python-requests-cache python-simplejson \
      python-yaml python-defusedxml python-markupsafe python-six python-whoosh
RUN pip install --upgrade pip wheel setuptools
RUN pip install git+git://github.com/IdentityPython/pyXMLSecurity.git#egg=pyXMLSecurity \
      && pip install git+git://github.com/IdentityPython/pyFF.git#egg=pyFF
RUN addgroup --gid ${GID} user && adduser --uid ${UID} --ingroup user --gecos user --disabled-password --home /app user
WORKDIR /app
USER user
