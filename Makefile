UID=$(shell id -u)
GID=$(shell id -g)
FEED := test.fd
XML  := $(FEED:.fd=.xml)
DOCKER_TAG := pyff-duplicates

all: $(XML)

$(XML): $(FEED) .image
	@docker run --rm -it -v $(PWD):/app $(DOCKER_TAG) pyff $<

.image: Dockerfile
	@docker build --build-arg UID=$(UID) --build-arg GID=$(GID) -t $(DOCKER_TAG) .
	@touch $@

.PHONY: clean
clean:
	@rm -rf test.xml test.*.xml

clean-image:
	rm -f .image
	docker rmi $(TAG)
