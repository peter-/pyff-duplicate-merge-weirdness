pyff duplicate entities weirdness
=================================

Running
-------

This repo includes everything to reproduce the issue: Test pyff config, test metadata, a `Dockerfile` for the required software with pyFF and pyXMLSecurity (on a minimal Debian 9 system), and a `Makefile` to tie it all together.

Requirements:

* Docker Engine (Docker CE)
* make

Running `make` will first build the Docker image, which may take a while. In the image will be created a local user and group with the uid and gid of the current user invoking `make`. Then it will run the image, mounting the current directory into the container to make the config files available to `pyff` inside the container as well as make the files created inside the container available to the host.

As the result is not always the same you may have to run `make` multiple times, touching `test.fd` in between to trigger the re-creation of the existing result in `test.xml`. The test feed contains a call to the `stats` pipeline to signal that the number of entities has changed. This makes it easy to stop at a certain point and copy the resulting file aside, before running `make` again until the numbers change, again:

```sh
$ touch test.fd ; make
---
total size:     5
selected:       4
          idps: 0
           sps: 4
---
$ cp test.xml test.OK.xml
```
```sh
$ touch test.fd ; make
---
total size:     5
selected:       5
          idps: 0
           sps: 5
---
$ cp test.xml test.FAIL.xml

```

Now compare `test.OK.xml` with `test.FAIL.xml`.  
See below for detailed descriptions of expected and observed results.

Test data
---------

A simple pyff feed is provided in `test.fd` to illustrate the problem(s). It references two test SAML 2.0 Metadata documents that are also provided:

`local.xml` has 3 entities, all registered by `http://example.org`:

```sh
$ fgrep entityID local.xml | sed -r 's/.*entityID="(.+)".*$/\1/'
http://test-sp-1
http://test-sp-2
http://test-sp-3
```

`remote.xml` also has 3 entities:

```sh
$ fgrep entityID remote.xml | sed -r 's/.*entityID="(.+)".*$/\1/'
http://test-sp-1
http://test-sp-4
http://test-sp-5
```
but from other sources:

* `http://test-sp-1` is registered by `http://OTHER.example.com`, i.e., this entity overlaps with one registed in `local.xml` but comes from another registrar.
* `http://test-sp-4` is registered by `http://example.net` and not of further significance here
* `http://test-sp-5` has no `mdrpi:RegistrationInfo` (and so should get filtered out by the XPath in the provided `feed.fd`).


Results
-------

The number of *expected* entities after aggregation is 4:  
test-sp-[1-4], where test-sp-1 should come from registrar `http://example.org`.

The number of actually resulting entities varies: Sometimes it's the expected result (4 entities and test-sp-1 from `http://example.org`) OR it results in 5 entities:

* test-sp-1 **BUT** registered by `http://OTHER.example.com`
* test-sp-1 BUT registered by `http://OTHER.example.com`, **AGAIN**
* test-sp-2
* test-sp-3
* test-sp-4, the latter 3 all as expected

